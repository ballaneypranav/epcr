#!/usr/bin/python3
# Fetching the required data for testing
# a. entrez NCBI fasta test file
# b. a primer data file
from Bio import Entrez

Entrez.email = 'unknown@debian.org'
database = 'nucleotide'
fn = 'data/gingko.fasta'

query = [
    '((chloroplast OR plastid) AND "complete genome" AND Embryophyta NOT (mi- tochondrion OR mitochondrial)) AND "Ginkgo biloba"'
]

primer = "rbcL1/rbcLA\t TTGGCAGCATTYCGAGTAACTCC\t CCTTTRTAACGATCAAGRC"


def fetch_ids(dbs, qr):
    # Fetch the query IDs
    print(dbs, qr)
    handle = Entrez.esearch(db=dbs, term=qr)
    record = Entrez.read(handle)
    ids = record['IdList']
    handle.close()

    # Fetch the first result
    ff = open(fn, 'w')

    handle_fasta = Entrez.efetch(
        db=dbs, id=ids[0], rettype='fasta', retmode='text')
    ff.write(handle_fasta.read().rstrip('\n'))

    handle_fasta.close()


def create_primer(pr):
    pf = open('data/rbcL-primer.txt', 'w')
    pf.write(pr)
    pf.write('\n')
    pf.close()


fetch_ids(database, query)
create_primer(primer)
